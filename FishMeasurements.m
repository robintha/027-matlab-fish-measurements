% class to define relevant functions for measurements on fish bodies
% author:   robin thandiackal
% date:     22.03.2018

classdef FishMeasurements < handle
    
properties
    image_          % cell array of images
    calib_length_   % reference length to determine scaling factor
    image_scale_    % scaling factor from pixel to real-world (mm)
    species_
    mcz_
    indiv_
    plane_
    
    curved_lines_   % cell array of curved lines (n_markers_ x 2) 
                    % line 1: ref (mid) line
                    % line 2: fork length
    dense_lines_    % cell array of interpolated curved lines (n_int_ x 2)
    straight_lines_ % cell array of straight lines (7 x 2)
                    % lines 1-6: depths at [10,25,50,75,90,100]% of body
                    % line 7: max body depth
                    
    current_curved_    % current curved line  
    current_dense_     % current dense line
    current_key_points_% current key points
    
    n_int_          % number of interpolation points on curved lines
    n_markers_      % number of markers along the midline
    key_point_frac_ % key point fractions e.g. [0.1,0.25,...]
    
    results_    % results
                % [midline_length,fork_length,depth1,...,depth6,...
                %  max_depth,max_depth_location]
    
    x_          % x-coordinates of current markers
    y_          % y-coordinates of current markers
                    
    midline_plot_      % handle to equidistant marker point plot
end

methods
    %% Constructor 
    % input_image:      image
    function obj = FishMeasurements(input_image)
        
        obj.image_ = input_image;
        
        obj.curved_lines_ = {};
        obj.dense_lines_ = {};
        obj.straight_lines_ = {};
        obj.current_curved_ = [];
        obj.current_dense_ = [];
        obj.current_key_points_ = [];
        obj.results_ = [];
        obj.n_int_ = 100;
        obj.key_point_frac_ = [0.1,0.25,0.5,0.75,0.9,1];
        
    end
    
    %% Calibration
    function SetImageScale(obj)
        DisplayImage(obj);
        
        obj.n_markers_ = input('Number of markers?\n');
        obj.calib_length_ = input('Calibration length in [mm]?\n');
        obj.species_ = input('Species? \n','s');
        obj.mcz_ = input('MZC number? \n','s');
        obj.indiv_ = input('Individual number \n','s');
        obj.plane_ = input('Dorsal or Lateral? \n','s');
        
        [p,dist_pix] = DefineStraightLine(obj,'b')
        obj.image_scale_ = obj.calib_length_/dist_pix;
        close;
    end
    
    %% Set different curved and straight lines
    function DefineAllLines(obj)
        
        % initialize figure position
        fig = figure();
        fig.Position = [0 0 1920 1080];
        
        % define midline
        DefineCurvedLine(obj);
        obj.curved_lines_{1} = obj.current_curved_;
        obj.dense_lines_{1} = obj.current_dense_;
        
        % midline length
        x_dense = obj.current_dense_(:,1);
        y_dense = obj.current_dense_(:,2);
        obj.results_(1) = sum(sqrt(diff(x_dense).^2+diff(y_dense).^2))...
                            * obj.image_scale_;
        
        % display key points
        KeyPoints(obj);
        hold on;
        HighlightKeyMarks(obj);
        plot(x_dense,y_dense,'-r');
        
        col1 = 'b';
        % set depth lines and compute depths
        for i = 1:length(obj.key_point_frac_)
            [p,len_pix] = DefineStraightLine(obj,col1);
            obj.straight_lines_{i} = p;
            obj.results_(i+2) = len_pix * obj.image_scale_;
        end
        
        col2 = 'g';
        % set max depth line and compute max depth
        [p,len_pix] = DefineStraightLine(obj,col2);
        obj.straight_lines_{length(obj.key_point_frac_)+1} = p;
        obj.results_(end+1) = len_pix * obj.image_scale_;
        
        % compute intersection of max depth line with midline
        pos = obj.straight_lines_{length(obj.key_point_frac_)+1}.Position;
        x = obj.dense_lines_{1}(:,1);
        y = obj.dense_lines_{1}(:,2);
        m = diff(pos(:,2))/diff(pos(:,1));
        q = pos(1,2)-m*pos(1,1);
        [~,ind] = min(abs(y-m*x-q));
        obj.results_(end+1) = obj.image_scale_ * ...
                sum(sqrt(diff(x(1:ind)).^2+diff(y(1:ind)).^2));
        
        % fork length curve
        SetMarkerPoints(obj); 
        obj.curved_lines_{2} = obj.current_curved_;
        obj.dense_lines_{2} = obj.current_dense_;
        
        % fork length
        x_dense = obj.current_dense_(:,1);
        y_dense = obj.current_dense_(:,2);
        obj.results_(2) = sum(sqrt(diff(x_dense).^2+diff(y_dense).^2))...
                            * obj.image_scale_;
         
    end
    
    %% Define straight line
    function [p,len_pix] = DefineStraightLine(obj,color)
        hold on;
        p = drawline;
        p.Color = color;
        pause;
        x = p.Position(:,1);
        y = p.Position(:,2);
        len_pix = sqrt((x(1)-x(2))^2+(y(1)-y(2))^2);
    end
       
    %% Define curved line
    function DefineCurvedLine(obj)
                
        markersize = 1;

        % visualization of tracking object
        DisplayImage(obj);
        ax = gca;
        xlimits = ax.XLim;
        ylimits = ax.YLim;
        MidlinePlotAppearance(obj,markersize);
        
        % set marker points
        SetMarkerPoints(obj);
        
    end
    
    %% Set markers to define the curved line
    % i:        current frame number
    % xlimits:  1 x 2 array of axis limits in x-direction
    % ylimits:  1 x 2 array of axis limits in y-direction
    function SetMarkerPoints(obj)
        
        % reload (if available) or set impoints
        markers = [];
        for j = 1:obj.n_markers_
            i_point(j) = impoint();
            markers(j,:) = i_point(j).getPosition;
        end
        obj.current_curved_ = markers;
        
        ComputeDenseCurvedLine(obj);
        DrawDenseCurvedLine(obj);
        
        for j = 1:obj.n_markers_
            addNewPositionCallback(i_point(j),...
                @(p) UpdateCurvedLine(obj,p,j));
        end
%         title(['Frame: ' num2str(i) '/' num2str(obj.n_frames_) ...
%                 ' - modify points if needed']);

        pause;
        hold off;
        obj.x_ = obj.current_curved_(:,1);
        obj.y_ = obj.current_curved_(:,2);
    end
    
    %% Compute curved line
    
    function ComputeDenseCurvedLine(obj)
        
        DenseMarkerInterpolation(obj);
                
    end
    
    %% Update curved line
    
    function UpdateCurvedLine(obj,p,j)
        
        obj.current_curved_(j,:) = p;
        
        ComputeDenseCurvedLine(obj);
        DrawDenseCurvedLine(obj);
        
    end
        
    
    %% (Dense) interpolation between markers
    % frame_id:    frame number
    function DenseMarkerInterpolation(obj)
        
        % number of interpolation points
        n_samples = obj.n_int_;
        
        % midline markers
        x = obj.current_curved_(:,1);
        y = obj.current_curved_(:,2);
        
        % interpolation
        q_dense = linspace(0,1,n_samples);
        q = linspace(0,1,length(x));
        obj.current_dense_(:,1) = interp1(q,x,q_dense,'spline');
        obj.current_dense_(:,2) = interp1(q,y,q_dense,'spline');
        
    end
    
    %% Find key point coordinates
    function KeyPoints(obj)
        
        key_points = obj.key_point_frac_;
        n_points = length(key_points);
                
        obj.current_key_points_ = [];
        
        x_dense = obj.current_dense_(:,1);
        y_dense = obj.current_dense_(:,2);
        
        % arc length along curve
        curve_length = sum(sqrt(diff(x_dense).^2+diff(y_dense).^2));
        cumsum_length = ...
            [0,cumsum(sqrt(diff(x_dense).^2+diff(y_dense).^2))'];
        
        % find equidistant marker positions along
        % interpolated curve
        for i = 1:n_points-1
            ind(i) = find(cumsum_length>=key_points(i)*curve_length,1);
        end
        ind(i+1) = length(cumsum_length);
        
        % store midline in class object
        obj.current_key_points_(:,1) = x_dense(ind);
        obj.current_key_points_(:,2) = y_dense(ind);
        
    end
    
    %% Display image
    function DisplayImage(obj)
        imshow(obj.image_);
        fig = gcf;
        fig.Position = [0 0 1920 1080];
        hold on;
        axis equal;
    end
    
    %% Midline plot appearance
    function MidlinePlotAppearance(obj,markersize)
        obj.midline_plot_ = plot(0,0,'-or','linewidth',2,...
                                 'markersize',markersize,...
                                 'markerfacecolor','r');
    end
    
    %% Highlight key marks
    function HighlightKeyMarks(obj)
        plot(obj.current_key_points_(:,1),obj.current_key_points_(:,2),...
            'or','markerfacecolor','r','markersize',10);
    end
    
    %% Draw dense curved line
    function DrawDenseCurvedLine(obj)
        obj.x_ = obj.current_dense_(:,1); 
        obj.y_ = obj.current_dense_(:,2);
        DrawMidline(obj);
    end
    
    %% Draw midline (e.g. dense, markers, etc.)
    function DrawMidline(obj)
        set(obj.midline_plot_,'xdata',obj.x_,'ydata',obj.y_);
    end
    
    %% Export
    function Export(obj,filename,image_name)
        fid = fopen(filename,'a');
        fprintf(fid,'%s,',image_name);
        fprintf(fid,'%s,%s,%s,%s,',...
            obj.species_,obj.mcz_,obj.indiv_,obj.plane_);
        for i = 1:length(obj.results_)
            fprintf(fid,'%03.5f',obj.results_(i));
            if(i<length(obj.results_))
               fprintf(fid,','); 
            end
        end
        fprintf(fid,'\n');
        fclose(fid);
    end
    
end

end
