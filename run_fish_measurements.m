% Script to load and carry out measurements
% author:   robin thandiackal
% date:     22.03.2019

close all;
clear all;

%% folder location with fish images
fish_folder = 'megakinematic_photos_used/';
file_type = 'jpg';
target_file_name = 'results.csv';
file_info = dir([fish_folder,'*.',file_type]);

%% header export file
if(isempty(dir(target_file_name)))
    fid=fopen(target_file_name,'w');
    fprintf(fid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n',...
            'filename','species','mcz_no','indiv_no','plane',...
            'midline_length','fork_length',...
            'depth10','depth25','depth50','depth75','depth90','depth100',...
            'max_depth','max_depth_location');
    fclose(fid);
else
    disp([target_file_name ' already exists.']);
    disp('Export will append to this file');
end

%% measurements

for i = 1:length(file_info)
    
    %% load images
    im = imread([fish_folder,'/',file_info(i).name]);
    
    %% init class (markers and calib)
    meas = FishMeasurements(im);
    
    %% image scale
    meas.SetImageScale();
    
    %% Define the different measurement lines
    meas.DefineAllLines();
    
    %% Export
    meas.Export(target_file_name,file_info(i).name);
    
    %%
    close;
    delete meas;
end

% Input in the beginning: 1) ask for calib distance, 2) ask for # markers
% Midline with interpolation points
% depth lines for [10,25,50,75,90,100]% of midline
% 1 line for maximum body depth + compute intersection length with midline
% Additional curved line (fork length), only absolute length required

